import {
  OPERACION_SUMA, OPERACION_RESTA, OPERACION_DIV, OPERACION_MULT,
} from './constants';


/**
 *
 * @param numero1 - Primero número a sumar
 * @param numero2 - Segundo número a sumar
 * @param operacion - sumar, restar, dividir, multiplicar
 */


function calc(numero1: any, numero2: any, operacion: string): any {
  if (numero1 === undefined
      || numero2 === undefined
      || typeof numero1 !== 'number'
      || typeof numero2 !== 'number') {
    throw new Error('Debe proporcionar un número');
  }
  let resultado: number;
  if (operacion === OPERACION_SUMA) {
    resultado = numero1 + numero2;
  }
  if (operacion === OPERACION_RESTA) {
    resultado = numero1 - numero2;
  }
  if (operacion === OPERACION_DIV) {
    resultado = numero1 / numero2;
  }
  if (operacion === OPERACION_MULT) {
    resultado = numero1 * numero2;
  }
  return resultado;
}

export { calc };
