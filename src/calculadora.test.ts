import { calc } from './calculadora';
import {
  OPERACION_SUMA, OPERACION_RESTA, OPERACION_DIV, OPERACION_MULT,
} from './constants';

describe('calculadora.test', () => {
  it('Probar la suma', () => {
    expect(calc(1, 2, OPERACION_SUMA)).toBe(3);
  });

  it('Probar la resta', () => {
    expect(calc(2, 1, OPERACION_RESTA)).toBe(1);
  });

  it('Probar la division', () => {
    expect(calc(4, 2, OPERACION_DIV)).toBe(2);
  });

  it('Probar la multiplicacion', () => {
    expect(calc(4, 2, OPERACION_MULT)).toBe(8);
  });

  it('probar que te mande un error si le mandas a la función valores no númericos', () => {
    expect(() => { calc('a', 'b', OPERACION_SUMA); }).toThrowError('Debe proporcionar un número');
  });

  it('probar que te mande un error si le mandas a la función valores no definidos', () => {
    expect(() => { calc('', '', OPERACION_SUMA); }).toThrowError('Debe proporcionar un número');
  });
});
