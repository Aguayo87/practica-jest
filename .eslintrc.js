module.exports = {
    env: {
        "node": true,
        "jest": true,
    },
    extends: [
        "airbnb-base"
    ],
    rules: {
        "import/extensions": [
            "error",
            "ignorePackages",
            {
                "js": "never",
                "jsx": "never",
                "ts": "never",
                "tsx": "never"
            },
        ],
        "import/prefer-default-export": "off",
    },
    parser: "@typescript-eslint/parser",
    parserOptions: {
        "ecmaVersion": 2018
    },
    plugins: [
        "@typescript-eslint"
    ],
    settings: {
        "import/resolver": {
            "node": {
                "extensions": [".js", ".jsx", ".ts", ".tsx"]
            }
        }
    },
}